# -*-encoding: utf-8 -*-
__author__ = 'luke'

class ConfReader:

    def __init__(self, path):
        self.__path = path
        self.__conf = dict()

        with open(path) as f:
            for line in f:
                key_value = line.split('=', 1)
                self.__conf[key_value[0]] = key_value[1].strip()

    def get_conf(self, key):
        return self.__conf[key]


