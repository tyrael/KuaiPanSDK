# -*-encoding: utf-8 -*-
__author__ = 'luke'

import os
import unittest
from urllib2 import HTTPError

from kuaipan import KuaiPan


class KuaiPanTest(unittest.TestCase):
    def setUp(self):
        self.__kp = KuaiPan("你的 oauth_token ", "你的 oauth_token_secret ")
    # 验证授权使用，使用方法为运行这个函数，然后打开输出地 URL，登录快盘，然后不需要做其他操作
    # 回到程序中，等到输出 oauth_token 和 oauth_token_secret，并将你的 oauth_token 和 token_secret 替换上面的两个
    # def test_auth(self):
    #     self.__kp = KuaiPan()
    #     temp_token = self.__kp.requestToken()
    #     auth_link = self.__kp.authorize(temp_token["oauth_token"])
    #     self.assertIn(r"https://www.kuaipan.cn/api.php?ac=open&op=authorise&oauth_token=", auth_link,
    #                   "You Build Error Auth Link")
    #     print auth_link
    #     time.sleep(10)
    #     self.__token = self.__kp.accessToken()
    #     self.__oauth_token = self.__token["oauth_token"]
    #     self.__oauth_token_secret = self.__token["oauth_token_secret"]
    #     print self.__token

    def test_metedata(self):
        meta_data = self.__kp.metadata('')
        self.assertIsNotNone(meta_data)

    def test_account_info(self):
        account_info = self.__kp.account_info()
        self.assertIn("max_file_size", account_info)

    def test_create_dir(self):
        dir_info = self.__kp.create_folder("test_folder")
        self.assertEqual(dir_info["path"], "/test_folder")

    def test_delete_dir(self):
        try:
            self.__kp.metadata("test_folder")
        except HTTPError:
            self.__kp.create_folder("test_folder")

        folder_info = self.__kp.metadata("test_folder")
        self.assertIsNotNone(folder_info)
        delete_info = self.__kp.delete("test_folder")
        self.assertEqual(delete_info["msg"], "ok")

    def test_upload(self):
        try:
            self.__kp.metadata("test.jpg")
            self.__kp.delete("test.jpg")
        except HTTPError:
            pass
        upload_info = self.__kp.upload("test.jpg", "/Users/luke/Pictures/test.jpg")
        self.assertIsNotNone(upload_info["file_id"])

    def test_download(self):
        try:
            self.__kp.metadata("test.jpg")
        except HTTPError:
            self.__kp.upload("test.jpg", "/Users/luke/Pictures/test.jpg")
        data = self.__kp.download("test.jpg")
        origin_size = os.path.getsize(r"/Users/luke/Pictures/test.jpg")
        self.assertEqual(origin_size, len(data))
