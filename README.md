##金山快盘SDK
这是一份金山快盘操作的SDK，基于 Python 编程语言。使用简单，只需要参考 test/KuaiPanTest.py 文件里面的代码即可。

###各文件介绍：
kuaipan.py：SDK 主文件，封装了金山快盘的各种操作

conf/app-info.conf：配置文件，里面放置你的应用的key 和 secret，内容格式如下：

    consumer_key=你的应用的key
    consumer_secret=你的应用的secret
            
lib/ConfReader.py：配置读取文件，用于读取应用的 key 和 secret

poster/*：用于辅助操作金山快盘的类

###最简易示例：
    
    # -*-coding:utf-8 -*-
    KuaiPan.consumer_key = '你的app key'
    KuaiPan.consumer_key_secret = '你的app key secret'

    kp= KuaiPan()
    tempToken = kp.requestToken()
    authLink = kp.authorize(tempToken["oauth_token"])
    print "请打开这个链接:\n"+authLink
    code = raw_input("请输入页面中的 code ..")
    token = kp.accessToken()
    print kp.account_info()
